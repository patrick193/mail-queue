<?php
//ini_set('error_reporting', E_ALL);
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);

require_once './src/autoloader.php';

use Controllers\ViewController\Main;
use Controllers\MainFunctions\Site\MainController as mc;

/////////////////// ///////////
//      pagination part     //
$pagination['limit'] = ( isset($_GET['limit']) ) ? $_GET['limit'] : 5;
$pagination['page'] = ( isset($_GET['page']) ) ? $_GET['page'] : 1;
$pagination['links'] = ( isset($_GET['links']) ) ? $_GET['links'] : 2;
//     end of pagination part   //
///////////////////  ////////////
//$method = new CheckRequest();
//$post = $method->GetPost('delete');
//$get = $method->GetGet('update');
if (!empty($_POST['delete'])) {
    $pars = new mc();
     $pars->delete($pars->ParseUrl($_POST['delete']));
//    header("Location: index.php");
}
$controller = new Main();
$controller->render($pagination);
