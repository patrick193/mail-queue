<?php


namespace Entity;


class Queue {

    /**
     * MailId
     * @var string $mail_id
     */
    private $mail_id;

    /**
     * Date time
     * @var \DateTime $date
     */
    private $date;

    /**
     * Sender full name
     * @var string $sender_full_name
     */
    private $sender_full_name;

    /**
     * like 'Postfix user id'
     * @var string $sender
     */
    private $sender;

    /**
     * @var string $to
     */
    private $to;

    /**
     * @var string $subject
     */
    private $subject;

    /**
     * @var string $xmailer
     */
    private $xmailer;
    
    /**
     *
     * @var string $from 
     */
    private $from;


    /**
     *
     * @var string $body 
     */
    private $body;

    /**
     * @var integer $deleted
     */
    private $deleted; 

    /**
     * @return string
     */
    public function getMailId()
    {
        return $this->mail_id;
    }

    /**
     * @param string $mail_id
     */
    public function setMailId($mail_id)
    {
        $this->mail_id = $mail_id;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getSenderFullName()
    {
        return $this->sender_full_name;
    }

    /**
     * @param string $sender_full_name
     */
    public function setSenderFullName($sender_full_name)
    {
        $this->sender_full_name = $sender_full_name;
    }

    /**
     * @return string
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param string $sender
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
    }

    /**
     * @return string
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param string $to
     */
    public function setTo($to)
    {
        $this->to = $to;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param string $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return string
     */
    public function getXmailer()
    {
        return $this->xmailer;
    }

    /**
     * @param string $xmailer
     */
    public function setXmailer($xmailer)
    {
        $this->xmailer = $xmailer;
    }
    
     public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param string $from
     */
    public function setFrom($from)
    {
        $this->from = $from;
    }
    
     public function getBody()
    {
        return $this->body;
    }

    /**
     * @param string $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     *@return $this->deleted
     */
    public function getDeleted(){
	return $this->deleted;
    } 
    
    /**
     * @param $deletd
     */
    public function setDeleted($deleted){
	$this->deleted = $deleted;
    }

}
