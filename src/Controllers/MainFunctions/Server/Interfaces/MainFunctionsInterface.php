<?php

namespace Controllers\MainFunctions\Server\Interfaces;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MainFunctionsInterface
 *
 * @author vladyslav
 */
interface MainFunctionsInterface {
    
    /**
     * To execute a shell command
     *
     * @param string $command
     */
    public function Execute($command);

    /**
     * function to get all mails
     */
    public function GetAllMails();
    /**
     * To parse returned data from function Execute
     * @param string $str
     */
    public function ParserMail($str);

    /**
     * to parse queue
     * @param string $str
     */
    public function ParserQueue($str);
    
    /**
     * To convert array to object type Queue
     * 
     */
    public function ToObject();

    /**
     * To write data to db
     * @param $data
     */
    public function ToDb();

}
