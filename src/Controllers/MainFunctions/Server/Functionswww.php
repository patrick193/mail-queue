<?php

namespace Controllers\MainFunctions\Server;

use Entity\Queue as queue;
use Controllers\MainFunctions\Server\Interfaces\MainFunctionsInterface as MI;
use Controllers\DB\Functions\DBFunctions;
use Controllers\ViewController\Pagination;

/**
 * Description of MainFunctions
 *
 * @author vladyslav
 */
class Functions implements MI {

    private $data = array(); // array for keep all date before we get an object
    protected $queue = array(); // array of objects
    public $db;

    public function __construct() {
        $this->db = new DBFunctions(\Configuration::$host, \Configuration::$user, \Configuration::$password, \Configuration::$db);
    }

    /**
     * function to get mails from db and paginate them
     * @param type $limit
     * @param type $page
     * @param type $links
     * @return type
     */
    public function MailsPagination() {

        $query = $this->db->select(array("from" => 'mails', "select" => array('*')));
        $paginator = new Pagination($this->db->connect(), $query);

        return $paginator;
    }

    /**
     * @param string $command
     * 
     */
    public function Execute($command) {
        if ($command) {
            $mails_not_filter = shell_exec($command);
            return $mails_not_filter;
        }
        return false;
    }

    /**
     * queue parser
     * @param string $str
     */
    public function ParserQueue($str) {
        $mail_id_array = array();
        $queue = explode("\n", $str);

        foreach ($queue as $mail_id) {
            $mail = preg_split("/\s/", trim($mail_id)); // split all string
            if (preg_match("/^\d*[A-Z]*\d/", $mail[0])) {
                $mail_id_array[] = $mail[0];
            }
        }
        return $mail_id_array;
    }

    /**
     * 
     * @param string $str
     */
    public function ParserMail($str) {
        if (!$str) // some short check
            return false;

        $message_content = preg_split("/\*\*\* MESSAGE CONTENTS /", $str); // split string and get a content
        $this->data["sender_fullname"] = $this->EasyFind($str, "sender_fullname"); // sender full name if the function will find it
        $this->data["Date"] = $this->EasyFind($message_content[1], "Date"); // date if the function will find it
        $this->data["To"] = str_replace(">", "", str_replace("<", "", $this->EasyFind($message_content[1], "To"))); // recipient if the function will find it
        $this->data["Subject"] = $this->EasyFind($message_content[1], "Subject"); // subject if the function will find it
        $this->data["From"] = str_replace(">", "", str_replace("<", "", $this->EasyFind($message_content[1], "From"))); // sender if the function will find it
        $this->data["X-Mailer"] = $this->EasyFind($message_content[1], "X-Mailer"); // X-Mailer if the function will find it
        $this->data["sender"] = $this->UserIdFinder($message_content[1]); // X-Mailer if the function will find it
        $this->data["MailBody"] = $this->GetMailBody($message_content[1], $this->DetermineMailType($message_content[1]));

        $this->NormalizationMailId($this->EasyFind($message_content[1])); // mail id if the function will find it and normalization id
        $this->ToObject(); //of course we should have object. one mail it is one object. in a result we will have array of objects
    }

    /**
     * we should determine type of e-mail. has mail html or not
     * @param string $text
     */
    public function DetermineMailType($text) {
        if (preg_match("/\<html/", $text)) {
            return 'html';
        } else {
            return 'not html';
        }
    }

    /**
     * function just find body
     * @param string $input
     */
    public function GetMailBody($input, $type) {
        $type == 'html' ? $reg = "<body" : $reg = "\n\n";
        $body = mb_stristr($input, $reg);
        if ($body) {
            if (preg_match("/^\n\D\W/", $body)) {
                $body = mb_stristr(substr($body, 1), $reg);
            }
            $body = substr(substr($body, stripos($body, "\n")),
                    0, stripos(substr($body, stripos($body, "\n")), "</body>"));// delete tag body.. 
            return trim($body);
        }
        return "We dont find mail body";
    }

    /**
     * fintion for normalization mail id
     * @param string $mail_id
     */
    public function NormalizationMailId($mail_id) {
        if ($mail_id == "none") {
            $this->data["mail_id"] = "dont_find";
        } else {
            $start = strrchr($mail_id, "/"); // start find id
            $start = substr($start, 1); //delete first symbol(it is '/')

            $end = stripos($start, " *"); // find the end of our id
            $mail_id = substr($start, 0, $end); // cut id
            $this->data["mail_id"] = $mail_id; // put into array
        }
    }

    /**
     * function for finding all easy parametrs separeted :
     * @param string $text
     * @param string $parametr
     * @return string
     */
    public function EasyFind($text, $parametr = null) {
        $split = array(); //array to split
        $parametr ? $split = preg_split("/$parametr\: /", $text) : $split[1] = $text; //check if parametr is not null the program will use it. if null -> find mail id
        $pos = strpos($split[1], "\n"); //first line
        if ($pos) {
            $line = trim(substr($split[1], 0, $pos)); //cut it
            return $line;
        }
        return "none"; // if 
    }

    /**
     * finde user id like "Postfix user id"
     * @param string $text
     * @return string
     */
    public function UserIdFinder($text) {
        $split = explode(")", explode("(", mb_stristr(preg_split("/Received:/", $text)[1], "("))[1]);
        return $split[0];
    }

    /**
     * set parametrs 
     */
    public function ToObject() {
        $qeue = new queue();

        $qeue->setMailId($this->data["mail_id"]);
        $qeue->setDate($this->data["Date"]);
        $qeue->setSender($this->data["sender"]);
        $qeue->setSenderFullName($this->data["sender_fullname"]);
        $qeue->setSubject($this->data["Subject"]);
        $qeue->setTo($this->data["To"]);
        $qeue->setXmailer($this->data["X-Mailer"]);
        $qeue->setFrom($this->data["From"]);
        $qeue->setBody($this->data["MailBody"]);

        array_push($this->queue, $qeue);
    }

    /**
     * @param  $data
     */
    public function ToDb() {
        $this->db->connect();
        if (is_null($this->queue)) {
            return false;
        }

        foreach ($this->queue as $key => $queue) {

            $check = $this->CheckBeforInsert($queue->getMailId());
            if ($check == true or $queue->getMailId() == "dont_find") {
                unset($this->queue[$key]);
                unset($key, $queue);
                $this->db->close;
                if (!is_null($this->queue)) {
                    $this->ToDb();
                }
                break;
            }

            $date = strtotime($queue->getDate());
            $date = date("Y-m-d H:i:s", $date);
            $parametrs = array('table_name' => 'mails',
                'insert' => array(
                    'mail_id' => "'" . $queue->getMailId() . "'",
                    'date' => "'" . $date . "'",
                    'sender_full_name' => "'" . $queue->getSenderFullName() . "'",
                    'sender_id' => "'" . $queue->getSender() . "'",
                    'to' => "'" . $queue->getTo() . "'",
                    'from' => '"' . $queue->getFrom() . '"',
                    'subject' => "'" . $queue->getSubject() . "'",
                    'xmailer' => "'" . $queue->getXmailer() . "'",
                    'body' => '"' . $queue->getBody() . '"',
            ));
            $q = $this->db->insert($parametrs);
            $this->db->execute($q);
        }
        $this->db->close();
    }

    /**
     * 
     * @param string $mail_id
     * @param \DB\Functions\DBFunctions $this->db
     * @return boolean
     */
    public function CheckBeforInsert($mail_id) {
        $res = $this->db->where($this->db->select(array(
                    'from' => 'mails',
                    'select' => array('mail_id'))), array(
            'where' => 'mail_id',
            'value' => $mail_id));

        $res = $this->db->limit($res, 1);
        $res = $this->db->execute($res);

        if (is_array($res)) {
            return true;
        }
        return false;
    }

    /**
     * get mail queue from server and parse all mails
     */
    public function GetAllMails($mails = null) {
//        $str = $this->Execute("mailq");
//        $mails_id = $this->ParserQueue($str);
        $mails_id = $this->ParserQueue($mails);

        foreach ($mails_id as $mail) {
            $file = file_get_contents("files/$mail");
            $this->ParserMail($file);

//            $this->ParserMail($this->Execute("postcat -q $mail"));
        }
    }

    /**
     * before close the class we shuld check our cnnection. if it is true we will close it
     */
    public function __destruct() {
        if ($this->db->ping()) {
            $this->db->close();
        }
    }

}
