<?php

namespace Controllers\MainFunctions\Site;

use Controllers\DB\Functions\DBFunctions;

use Controllers\ViewController\Pagination;

/**
 * 
 * @author vladyslav
 */
class MainController {

    protected $db;

    public function __construct() {
        $this->db = new DBFunctions(\Configuration::$host, \Configuration::$user, \Configuration::$password, \Configuration::$db); // initialization DB class
    }

    /**
     * function to get pagination
     * @return type
     */
    public function MailsPagination() {

        $query = $this->db->select(array("from" => 'mails', "select" => array('*'))); // select all
        $q = $this->db->where($query, array('where' => 'deleted', 'value' => 0));//where not deleted
        $paginator = new Pagination($this->db->connect(), $q); // pagination

        return $paginator;
    }

    /**
     * it's function for split post or get query.
     * we chose  a separator a ';'
     * @param srting $url_parametres
     * @return array $arr
     */
    public function ParseUrl($url_parametres) {
        if (empty($url_parametres)) {
            return false;
        }
        $arr = explode(",", $url_parametres);
        return $arr;
    }

    /**
     * function for delete mails from db
     * @param array $parametres
     */
    public function delete($parametres) {
        if (empty($parametres)) {
            return false;
        }
        $this->db->ping() ? : $this->db->connect();
        foreach ($parametres as $id) {
            $update = array('deleted' => 1);
            $q = $this->db->update(array('update_set' => $update, 'table' => 'mails'));
	    $q = $this->db->where($q, array('where' => 'mail_id', 'value' => $id));
            $this->db->execute($q);


//            $this->Execute("postsuper -d $id");
        }
        $this->db->close();
    }



}
