<?php

namespace Controllers\DB\Functions;

use Entity\Queue;

/**
 * Description of Functions
 *    class sql query
 * @author vladyslav
 */
class DBFunctions {

    private $link;
    private $host, $username, $password, $database;

    public function __construct($host, $username, $password, $database) {
        $this->host = $host;
        $this->username = $username;
        $this->password = $password;
        $this->database = $database;
    }

    /**
     * connect to db
     * @return type
     */
    public function connect() {
        $this->link = mysqli_connect($this->host, $this->username, $this->password, $this->database);
//                OR die("There was a problem connecting to the database.");

        if ($this->link->connect_error) {
            die("Connection failed: " . $this->link->connect_error);
        }

        return $this->link;
    }

    /**
     * close connection
     * @return boolean
     */
    public function close() {
        if (!$this->link) {
            mysqli_close($this->link);
            unset($this->link);
            return true;
        }
        return false;
    }

    /**
     *  functio to select from db
     * @param array $parametrs
     */
    public function select($parametrs) {
        $what = "";

        if ($parametrs["select"][0] != "*") {
            for ($i = 0, $c = count($parametrs["select"]); $element = each($parametrs["select"]); $i++) {
                $what .= "`" . $element["value"] . "`";
                $i == ($c - 1)? : $what .= ",";
            }
            unset($element);
        } else {
            $what = "*";
        }
        $from = $parametrs["from"];
        $q = "SELECT " . $what . " FROM `" . $from . "`";
        return $q;
    }

    /**
     * query + where
     * @param string $query
     * @param array $param
     * @return string
     */
    public function where($query, $param) {
        $q = $query . " WHERE `" . $param["where"] . "` = '" . $param["value"] . "'";
        return $q;
    }

    /**
     * sql limit
     * @param string $query
     * @param integer $limit
     * @return type
     */
    public function limit($query, $limit = 1) {
        return $query . " LIMIT " . $limit;
    }

    /**
     * function to update data
     * @param array $parametrs
     * @return string
     */
    public function update($parametrs) {
        $set = "";
        for ($i = 0, $c = count($parametrs["update_set"]); $element = each($parametrs["update_set"]); $i++) {
            $set .= "`" . $element['key'] . "`='" . $element['value']."'";
            $i == ($c - 1)? : $set .= ",";
        }
        unset($element);
        $q = "UPDATE `" . $parametrs['table'] . "` SET " . $set;
        return $q;
    }

    /**
     * it is a first function to create a query. but tests showed this function too slow
     * i keep it for exaple
     * @param array $parametrs
     * @return string
     */
    public function insert_slow($parametrs) {
        $start = microtime();

        $values = "";
        $fields = "";
        $iteration = 0;
        $count = count($parametrs["insert"]) - 1;
        foreach ($parametrs["insert"] as $key => $value) {
            $fields .= "'" . $key . "'";
            $values .= $value;
            $iteration == $count? : $fields.="," and $values .=",";
            $iteration++;
        }
        unset($value, $key);
        $q = "INSERT INTO " . $parametrs["table_name"] . "($fields) VALUES $values";
        $end = microtime();
        var_dump($end - $start);
        var_dump($q);
        return $q;
    }

    /**
     * function for create query to insert it
     * @param array $parametrs
     * @return string
     */
    public function insert($parametrs) {

        $values = "";
        $fields = "";
        for ($i = 0, $c = count($parametrs["insert"]); $element = each($parametrs["insert"]); $i++) {
            $fields .= "`" . $element['key'] . "`";
            $values .= $element['value'];
            if (!($i >= ($c - 1))) {
                $fields .= ",";
                $values .= ",";
            }
        }
        unset($element);
        $q = "INSERT INTO `" . $parametrs["table_name"] . "`($fields) VALUES ($values)";
        return $q;
    }

    /**
     * to delete something from table
     * @param array $parametrs
     * @return string
     */
    public function delete($parametrs) {
        $q = "DELETE FROM `" . $parametrs["table"] . "` WHERE `" . $parametrs['where'] . "`='" . $parametrs['value'] . "'";
        return $q;
    }

    /**
     * 
     * @param string $query
     */
    public function execute($query) {
        if (!is_null($this->link)) {
            $result = mysqli_query($this->link, $query);
            if ($result->num_rows > 0) {
                $objects = array();
                while ($row = $result->fetch_object()) {//get object type queue
                    $r = new Queue();
                    $r->setBody($row->body);
                    $r->setDate($row->date);
                    $r->setFrom($row->from);
                    $r->setMailId($row->mail_id);
                    $r->setSender($row->sender_id);
                    $r->setSenderFullName($row->sender_full_name);
                    $r->setSubject($row->subject);
                    $r->setTo($row->to);
                    $r->setXmailer($row->xmailer);
		    $r->setDeleted($row->deleted);
                    $objects[] = $r;
                }unset($row, $r);
                return $objects;
            } else {
                return TRUE;
            }

            return false;
        }
    }

    /**
     * ping connect
     * @return boolean
     */
    public function ping() {
        return mysqli_ping($this->link);
    }

}
