<?php

namespace Controllers\ViewController;

use Controllers\MainFunctions\Site\MainController;

/**
 * main controller between view and model
 *
 * @author vladyslav
 */
class Main {

    protected $path = './web/';
    protected $mails, $pagination;

    public function __construct() {
        
    }

    public function render($pagination) {
        $res = new MainController();

        $this->pagination = $res->MailsPagination();// pagination
        $this->mails = $this->pagination->getData(abs((int) $pagination['limit']), abs((int) $pagination['page']));//get data pagination
        $this->pagination = $this->pagination->createLinks($pagination['links'], 'pagination pagination-li');
        $body = "";
        session_start();
        foreach ($this->mails->data as $mail) {
            $mail_body = $mail->getBody();
            $id = $mail->getMailId();
            $_SESSION['mail_body'][$id] = $mail_body;
            $body .= "<div class='z'>\n"
                    . "<div class='mail_header' id='".$id."'>

                            <input type='checkbox' class='mail_header_checkbox'>

                        <div class='mail_theme'>\n
                        " . $mail->getSubject() . "</div>\n"
                    . "<div class='mail_author'>\n" . $mail->getFrom()
                    . "</div>\n<div class='mail_date'>\n" . $mail->getDate() . "</div></div>\n"
                    . "<div class='mail_body'><iframe src='/src/frame.php?id=$id' width=100% height=100%  ></iframe></div></div>\n"
                    . ""
            ;
        }

//        session_destroy();
        $template = file_get_contents($this->path . "index.html");
        $template = str_replace("##mails##", $body, $template);
        $template = str_replace("##pagination##", $this->pagination, $template);
        echo $template;
    }

}
