# MAILQUEUE MANAGER #
Manger to read, keep, display and delete mails from mail queue.
The program is consist of two parts. There are server part and web interface.
## FEATURES ##
* server part independent  of web interface
* two parts connecting only across DataBase
* height level of security
* don't  need to install special libraries

## DataBase ##
The database are consist only of one table. On this table we are save all parameters what we need. 

### Table 'mails': ###
* mail_id - key
* date - date of sending
* sender_full_name - full name of sender
* to - recipient
* from - sender
* subject - subject of mail
* xmailer - X-Mailer
* body - body of the mail
* deleted - mark for deleting

As for last column. It need to mark the mail that it should be deleted from server. When the script from server part will see this mark the script will delete mail from queue and db. 

# SERVER PART #
### INSTALL SERVER PART IN THE PROJECT ###
To install in other project just add in composer
```composer
{
    "name": "mailqueue_server",
    "version": "dev-master",
    "dist": {
        "url": "https://patrick193@bitbucket.org/patrick193/mail-queue-server.git"
    }
}
```
### ROUTING ###
All this part should be located in
```html
/root/mailqueue/webuser
```
###  DESCRIPTION AND WORKING ###
First of all you need to change Configuration file to connect to the DataBase. The Configuration.php located on the root directory of server part of project.
### Configuration.php ###
```php
class Configuration {

    public static $host = "localhost";
    public static $user = "web4";
    public static $password = "main$151";
    public static $db = "usr_web4_1";

}
```
Change data what you need(host to db connection, user to db, user password and name of database).
### CRONJOB AND START SCRIPT ###
File Controllers/MainFunctions/Server/Functions.php must have a root access.
Server part of project should working with cron.
To open crontabs you have to type into terminal:
```html
$ crontab -e
```
After opening crontabs add new job:
```html
*/5 * * * * /usr/bin/php /root/mailqueue/webuser/Controllers/MainFunctions/Server/Cron.php
```
This is comand will start Cron.php file every 5 minutes.
If you want just start a script once type in terminal:
```html
$ /usr/bin/php /root/mailqueue/webuser/Controllers/MainFunctions/Server/Cron.php
```
### Structure of the server part: ###
* Entity/
* Controllers/DB/Functions/
* Controllers/MainFunctions/Server/
* Controllers/MainFunctions/Server/Interfaces/

In Entity directory is located an Entity of the table 'mails' in db.
Main functions of script work are located in 
```html
Controllers/MainFunctions/Server/Functions.php
```
If you want to override this class you must adhere to rules what installed in
```html
 Controllers/MainFunctions/Server/Interfaces/MainFunctionsInterface.php
```
Working with database are on the 
```html
Controllers/DB/Functions/DBFunctions.php
```
This is class help to generate different types of the query and execute them. 
### The features of using my DBF class are: ###
* the function execute are returned the object type \Entity\Queue::class, so you always working only with objects
* you have to call one functions with only one easy array and you are already have a query
* you can combine some queries only using php without mysql
 
# WEB INTERFACE #

### INSTALL IN THE PROJECT ###
If you want to add this part in your own project or change functions just add in composer
```composer
{
    "name": "mailqueue_web",
    "version": "dev-master",
    "dist": {
        "url": "https://patrick193@bitbucket.org/patrick193/mail-queue.git"
    }
}
```

## DESCRIPTION AND WORKING ##
Primarily you have to change file configuration to db access.
```php
#src/Configuration.php
class Configuration {

    public static $host = "localhost";
    public static $user = "web4";
    public static $password = "main$151";
    public static $db = "usr_web4_1";

}
```
Change host, db user, password for this user and name of db.

### STRUCTURE OF THE PROJECT ###
The project has two main directories:

* web/
* src/
In web directory are located only view files such as html files, js, css.
Directory src/ has all functional part. In src/ directory are located :
* Entity dir
* Controllers dir
* Configurations.php
* autoloader.php
* frame.php

### CONTROLLERS DIRECTORY ###
It is a content of src/Controller directory

* DB/Functions/
* Errors/
* MainFunctions/Site/
* ViewController/

So, first point. In this directory are located DBF class for easy working with db.
Second point. It is a directory has class for display errors.
Third point. Directory for main functions to the site.
And the last one. There are two classes to processing and display information in view.